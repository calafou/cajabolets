#include <Wire.h>
#include "RTClib.h"
#include <SD.h>
#include <SPI.h>
#include <SeeedOLED.h>
#include "Seeed_BME280.h"

BME280 bme280;
RTC_DS1307 rtc;
#define RELAY1 7                                            // señal del relay al pin D7
const int chipSelect = 10;                                 //  SD logger
File logfile;


void setup(){
  rtc.begin();
 if (!SD.begin(chipSelect))
  SeeedOled.init();                                       // Inicializa la pantalla OLED
  if(!bme280.init()){
    SeeedOled.putString("Sensor error");
  }
  pinMode(RELAY1, OUTPUT); 
  rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));        // Ponemos en hora el reloj, solo la primera vez, luego comentar y volver a cargar el sketch en la arduino.
}


void loop()
{

  DateTime now = rtc.now();
  
  if (bme280.getTemperature() >=24 || bme280.getHumidity() >=99){
    digitalWrite(RELAY1,LOW);
  }
  else{
    digitalWrite(RELAY1,HIGH);
  }
  delay(1000);
  File dataFile = SD.open("datalog.csv", FILE_WRITE);
  if (dataFile) {
    dataFile.print(now.year());
    dataFile.print('/');
    dataFile.print(now.month());
    dataFile.print('/');
    dataFile.print(now.day());
    dataFile.print(" ");
    dataFile.print(now.hour());
    dataFile.print(':');
    dataFile.print(now.minute());
    dataFile.print(':');
    dataFile.print(now.second());
    dataFile.println();
    dataFile.print(bme280.getHumidity());
    dataFile.print(',');
    dataFile.print(bme280.getTemperature());
    dataFile.print(',');
    dataFile.print(bme280.getPressure());
    dataFile.print(',');
    dataFile.close();
    SeeedOled.clearDisplay();                         
    SeeedOled.setNormalDisplay();                    
    SeeedOled.setPageMode();                        
    SeeedOled.setTextXY(2,0);                        
    SeeedOled.putString ("Presion   ");
    SeeedOled.putNumber (bme280.getPressure());
    SeeedOled.setTextXY(4,0);
    SeeedOled.putString ("Humedad      ");
    SeeedOled.putNumber (bme280.getHumidity());
    SeeedOled.setTextXY(6,0);
    SeeedOled.putString ("Temperatura  ");
    SeeedOled.putNumber (bme280.getTemperature());
    delay(10000);
    }
  }
