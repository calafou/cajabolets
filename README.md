# CajaBolets
Proyecto de monitorización de cultivos de la comunidad Calafou.org basado en arduino.
**[Wiki del proyecto](https://wiki.calafou.org/index.php/Soberan%C3%ADa_micol%C3%B3gica)**
# COMPONENTES
*  Arduino Uno
*  DHT11
*  Soil moisture sensor
*  Light sensor
*  Ventilador
# LIBRERIAS
*  [DHTlib](https://github.com/RobTillaart/Arduino/tree/master/libraries/DHTlib)
*  [RTClib](https://github.com/adafruit/RTClib)
*  [Wire](https://github.com/PaulStoffregen/Wire)